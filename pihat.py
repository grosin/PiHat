
from mux import multiplex
from adc import adc
import sys
import os

#check the address of the multiplexer
os.system('i2cdetect -y 1')

#I'm assuming its 0x73, replace it if nexessary
pihat_plexer=multiplex(1,0x73)
pihat_adc=adc(vref=2.8,bus=1,address=0x33)

pihat_plexer.set_adc()

#check that adc was turned on
os.system('i2cdetect -y 1')


NTC_voltage=[]
for channel in range(8):
    voltage=pihat_adc.read_voltage(channel)
    NTC_voltage.append(voltage)

print(NTC_voltage)
